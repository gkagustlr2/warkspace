let date = new Date();
switch (date.getMonth() + 1){ // 현재 날자
    case 12: //값
    case 1:
    case 2:
        console.log("겨울") //실행문
        break;
    case 3:
    case 4:
    case 5:
        console.log("봄")
        break;
    case 6:
    case 7:
    case 8:
        console.log("여름")
        break;
    case 9: 
    case 10: 
    case 11:
        console.log("가을")
        break; 
    default:
        console.log("어떤 행성에 살고 있니?")
        break;
}
//변수를 선언합니다.
let numberA = 52;
let numberB = 0;
let numberC = -23;
//조건을 구분합니다.
console.log(`${numberA}은/는 ${numberA > 0 ? "0 보다 큰" : " 0 또는 0보다 작은"} 숫자입니다.`); 
console.log(`${numberB}은/는 ${numberB > 0 ? "0 보다 큰" : " 0 또는 0보다 작은"} 숫자입니다.`);
console.log(`${numberC}은/는 ${numberC > 0 ? "0 보다 큰" : " 0 또는 0보다 작은"} 숫자입니다.`);

function calcSum(n){
    let sum = 0;
    for(let i = 1; i <= n; i++){
        sum += i;
    }
    console.log(`1부터 ${n}까지 더하면 ${sum}입니다.`);
}
calcSum(185);

function calcSum(n){ // n; 매개변수
let sum = 0;
for(let i = 1; i <=n; i++){
    sum += i;
} return sum;
}
let num = parseInt(prompt("몇까지 더할까요?"));
document.write(`1부터 ${num}까지 더하면 ${calcSum(num)}`)