const title = document.querySelector("#title");

//title.oneclick = () => {
//title.classList.add("clicked");
//}
title.onclick = () =>{
    if(!title.classList.contains("clicked")){
        title.classList.add("clicked");
    }else{
        title.classList.remove("clicked");
    }
}
//getElement~ 함수
//예전부터 사용하던 방법
//id 나 class, 태그명을 사용해 접근
//getElementById( )
//document.getElementById("id명")
//getRlementsByClassName( )
//document.getElementsByClassName("클래스명")
//getElementsByTagName( )
//document.getElementsByTagName("태그명")
//getElement*() 와 querySelector()의 차이
//id값이나 class값, 태그 이름을 사용해서 접근하는 것은 같다
//querySelector()를 사용하면 둘 이상의 선택자를 조합해서 접근할 수 있음(예,#detail > p)