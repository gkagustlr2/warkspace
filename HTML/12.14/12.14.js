let hi = () => '안녕하세요?'; // 생략한 형태
hi();

//화살표 함수 - 매개변수가 있을 때 --------------------------------------
let hi = function(user) {
    console.log(`${user}님, 안녕하세요?`);
}
hi("홍길동");

let hi = user => console.log(`${user}님, 안녕하세요?`);
hi("홍길동");

let sum = function(a,b) {
    return a + b;
}
sum(10,20);

let sum = (a, b) => a + b;
sum(10,20);

//배열을 생성합니다.
let array = [1,2,3,4,5,6];
//요소의 길이를 출력합니다.
for (let i = array.length - 1; i >= 0 ; i--){
    console.log(array[i]);
}

const bttn = document.querySelector("button");

function display(){
alert("클릭");}
bttn.addEventListener("click",display);

const bttn = document.querySelector("button"); //버튼 요소 가져옴

bttn.addEventListener("click",()=>{alert("클릭");});